import os
from pathlib import Path

from discord import Activity, __version__
from discord.ext import commands
from bot.cogs import ActiveNewsChannels, NewsSender, Query

bot = commands.Bot(command_prefix="!O ")
token = os.getenv("DISCORD_TOKEN") or Path("./token.secret").read_text()

bot.add_cog(ActiveNewsChannels(bot))
bot.add_cog(NewsSender(bot))
bot.add_cog(Query(bot))


@bot.event
async def on_ready():
    print(f"Logged in as: {bot.user.name} - {bot.user.id}\nVersion: {__version__}\n")
    await bot.change_presence(
        activity=Activity(name="Collecting intelligence...", type=0)
    )
    print(f"Successfully logged in and booted...!")


bot.run(token)
