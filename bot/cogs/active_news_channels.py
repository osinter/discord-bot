from discord import Embed
from discord.ext import commands

from .. import db, Location
from peewee import DoesNotExist


class ActiveNewsChannels(
    commands.Cog,
    name="Manage Active News Channels",
    description="Manage which channels on this server recieves news from OSINTer. Commands only available to server- or bot-owner.",
):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        brief="Adds the channel to list of active news channels in this server.",
        help="Using this command to add a channel to a the list of active news channels will make this bot send news from OSINTer in that channel.",
    )
    async def init(self, ctx, channel: commands.TextChannelConverter):
        try:
            Location.get(guild_id=ctx.guild.id, channel_id=channel.id)
            await ctx.channel.send(
                f'**"{channel.name}"** is already listed as an active channel in ***"{ctx.guild.name}"***'
            )
        except DoesNotExist:
            Location(
                guild_id=ctx.guild.id,
                channel_id=channel.id,
                guild_name=ctx.guild.name,
                channel_name=channel.name,
            ).save()
            await ctx.channel.send(
                f'Successfully added **"{channel.name}"** to the list of active channels in ***"{ctx.guild.name}"***'
            )

    @commands.command(
        brief="Removes channel from list of active news channels on this server"
    )
    async def remove(self, ctx, channel: commands.TextChannelConverter):
        try:
            Location.get(guild_id=ctx.guild.id, channel_id=channel.id).delete_instance()
            await ctx.channel.send(
                f'Successfully removed **"{channel.name}"** from the list of active channels in ***"{ctx.guild.name}"***'
            )
        except DoesNotExist:
            await ctx.channel.send(
                f'**"{channel.name}"** is not on the list of active channels in ***"{ctx.guild.name}"***'
            )

    @commands.command(brief="Remove all active news channels for this server")
    async def remove_all(self, ctx):
        Location.delete().where(Location.guild_id == ctx.guild.id).execute()

        await ctx.channel.send(
            f'Successfully removed all active channels in ***"{ctx.guild.name}"***'
        )

    @commands.command(brief="List active news channels for thiss server")
    async def list(self, ctx):
        channels = [
            location.channel_name
            for location in Location.select().where(Location.guild_id == ctx.guild.id)
        ]

        if channels:
            message = Embed(
                title="List of currently active news-channels:", color=0xFF6363
            )
            message.add_field(name=ctx.guild.name, value=" | ".join(channels))
            await ctx.channel.send(embed=message)
        else:
            await ctx.channel.send(
                "There's currently no active news channels on this server."
            )

    async def cog_command_error(self, ctx, error):
        print(error)
        if isinstance(error, commands.BadArgument):
            await ctx.channel.send("Channel not found")
        elif isinstance(error, commands.MissingPermissions):
            await ctx.channel.send(
                f"You are missing {', '.join(error.missing_perms)} permission(s) to run this command."
            )

    async def cog_check(self, ctx):
        if (
            not await ctx.bot.is_owner(ctx.author)
        ) and not ctx.guild.owner_id == ctx.author.id:
            raise commands.MissingPermissions(["admin"])
        return True
