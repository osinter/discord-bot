from discord.ext import commands, tasks

from .. import ROOT_URL
from ..formatting import format_list_of_articles

import urllib.parse
import requests


class Query(
    commands.Cog,
    description="Query news from OSINTer using different options. Commands available to everyone.",
):
    def __init__(self, bot):
        self.bot = bot

    def _search(self, params):
        query_string = urllib.parse.urlencode(
            params, quote_via=urllib.parse.quote, safe="", doseq=True
        )
        return requests.get(f"{ROOT_URL}articles/overview/search?{query_string}").json()

    @commands.command(
        brief="Sorts search results by how well they match",
        help="Searches OSINTer for news that matches the search queries, and return the 10 results that match the best. Case insensitive.",
    )
    async def search(self, ctx, *keywords):
        search_string = " ".join(keywords)
        search_results = self._search({"searchTerm": search_string, "limit": 10})

        await ctx.channel.send(
            embed=format_list_of_articles(search_results, search_string)
        )

    @commands.command(
        brief="Sorts search results that match by publish date",
        help="Searches OSINTer for news that matches the search queries, and return the 10 results that was most recently released. Case insensitive.",
    )
    async def search_recent(self, ctx, *keywords):
        search_string = " ".join(keywords)
        search_results = self._search(
            {
                "searchTerm": search_string,
                "sortOrder": "desc",
                "sortBy": "publish_date",
                "limit": 10,
            }
        )

        await ctx.channel.send(
            embed=format_list_of_articles(search_results, search_string)
        )
