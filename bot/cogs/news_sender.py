from discord import Embed
from discord.ext import commands, tasks

from .. import db, Location, SentPost, NEWS_FEED_URL, MINUTES_BETWEEN_NEWS_SEND
from ..formatting import format_single_article
from peewee import DoesNotExist

import requests
import asyncio
from datetime import datetime


class NewsSender(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.send_news.start()

    def collect_news(self):
        post_collection = requests.get(NEWS_FEED_URL).json()
        post_collection.reverse()

        already_sent_post_ids = [post.post_id for post in SentPost.select()]

        for post in post_collection:
            if post["id"] not in already_sent_post_ids:
                return post

        return None

    @tasks.loop(minutes=MINUTES_BETWEEN_NEWS_SEND)
    async def send_news(self):
        channels = [
            self.bot.get_channel(location.channel_id) for location in Location.select()
        ]

        post = self.collect_news()

        if post:
            formatted_post = format_single_article(post)

            coroutines = [channel.send(embed=formatted_post) for channel in channels]
            await asyncio.gather(*coroutines)

            SentPost(post_id=post["id"], sent_time=datetime.utcnow()).save()

    @send_news.before_loop
    async def wait_for_boot(self):
        await self.bot.wait_until_ready()
