from . import MAIN_COLOR, ROOT_URL
from discord import Embed

from datetime import datetime
import dateutil.parser


def cut_string(string, length):
    return (string[: (length - 3)].strip() + "...") if len(string) > length else string


def format_datetime(article_datetime):
    if not isinstance(article_datetime, datetime):
        try:
            article_datetime = dateutil.parser.isoparse(article_datetime)
        except ValueError:
            return article_datetime.split("T")

    return [article_datetime.strftime("%d, %b %Y"), article_datetime.strftime("%H:%M")]


def format_single_article(article):
    description = ""

    for text_part in article["description"].split("."):
        if not (len(description) + len(text_part)) > 250:
            description += text_part + "."
        else:
            description += ".."
            break

    message = Embed(
        title=f"OSINTer | {article['title']}",
        url=f"{ROOT_URL}#{article['id']}",
        color=MAIN_COLOR,
    )
    message.set_thumbnail(url=article["image_url"])
    message.add_field(name=description, value=article["url"], inline=False)
    message.add_field(
        name="Details: ",
        value="**Source:** "
        + article["source"]
        + "\n"
        + "**Date:** "
        + " | *".join(format_datetime(article["publish_date"]))
        + "*",
        inline=False,
    )
    return message


def format_list_of_articles(articles, search, nr_of_articles=10):
    message = Embed(
        title=f'List of news-article matching: *"{cut_string(search, 35)}"*',
        color=MAIN_COLOR,
    )

    if len(articles) == 0:
        message.add_field(
            name="Unfortunately no results was found",
            value="*Try again, using different keywords*",
        )
    else:
        for i, article in enumerate(articles[:nr_of_articles]):
            title = cut_string(article["title"], 55)
            date, time = format_datetime(article["publish_date"])

            message.add_field(
                name=f"**{date}** | *{time}*",
                value=f'*{i + 1}* - [{title}]({ROOT_URL}#{article["id"]})',
                inline=False,
            )

    return message
