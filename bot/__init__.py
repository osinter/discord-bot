from peewee import SqliteDatabase, Model, CharField, BigIntegerField, DateTimeField

import os
import logging

#########################################
##### Constants used elsewhere in the bot
#########################################
ROOT_URL = "https://osinter.dk/"
NEWS_FEED_URL = f"{ROOT_URL}articles/overview/newest"
MINUTES_BETWEEN_NEWS_SEND = 1
MAIN_COLOR = 0x00FF00


########################################
##### Configure DB used for storing data
########################################
db = SqliteDatabase(
    "data.db",
    pragmas={
        "journal_mode": "wal",
        "cache_size": -1 * 64000,  # 64MB
        "foreign_keys": 0,
        "ignore_check_constraints": 0,
        "synchronous": 2,
    },
)


class Location(Model):
    guild_id = BigIntegerField()
    channel_id = BigIntegerField()

    guild_name = CharField()
    channel_name = CharField()

    class Meta:
        database = db


class SentPost(Model):
    post_id = CharField()
    sent_time = DateTimeField()

    class Meta:
        database = db


db.connect()
db.create_tables([Location, SentPost])
db.close()


########################
###### Configure loggers
########################
if not os.path.exists("./logs"):
    os.makedirs("./logs")

loggerFormat = logging.Formatter(
    "[%(asctime)s] %(levelname)s in %(module)s: %(message)s"
)

discordLogger = logging.getLogger("discord")
discordLogger.setLevel(logging.DEBUG)
discordHandler = logging.FileHandler(
    filename="logs/discord.log", encoding="utf-8", mode="w"
)
discordHandler.setFormatter(loggerFormat)
discordLogger.addHandler(discordHandler)

logHandlers = {
    "printHandler": {"handler": logging.StreamHandler(), "level": logging.DEBUG},
    "fileHandler": {
        "handler": logging.FileHandler("logs/info.log"),
        "level": logging.INFO,
    },
    "errorHandler": {
        "handler": logging.FileHandler("logs/error.log"),
        "level": logging.ERROR,
    },
}

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

for handlerName in logHandlers:
    logHandlers[handlerName]["handler"].setFormatter(logHandlers[handlerName]["level"])
    logger.addHandler(logHandlers[handlerName]["handler"])
